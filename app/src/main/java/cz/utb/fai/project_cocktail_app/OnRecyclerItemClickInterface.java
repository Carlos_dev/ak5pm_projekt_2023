package cz.utb.fai.project_cocktail_app;

import android.view.View;

public interface OnRecyclerItemClickInterface {

    void onRecyclerItemClick(View view, int position, DrinkDataModel data);


}
