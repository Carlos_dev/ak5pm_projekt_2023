package cz.utb.fai.project_cocktail_app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DrinkData_recycler_view_adaptor extends RecyclerView.Adapter<DrinkData_recycler_view_adaptor.MyViewHolder> {


    Context context;

    ArrayList<DrinkDataModel> data;

    OnRecyclerItemClickInterface onItemClickListener;

   public DrinkData_recycler_view_adaptor (Context context, ArrayList<DrinkDataModel> data) {
        this.context = context;
        this.data = data;
   }

    public void setOnItemClickListener(OnRecyclerItemClickInterface onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public DrinkData_recycler_view_adaptor.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.drink_recycler_row, parent, false);

        return new DrinkData_recycler_view_adaptor.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DrinkData_recycler_view_adaptor.MyViewHolder holder, @SuppressLint("RecyclerView") int position) {

       holder.drinkTitle.setText(data.get(position).getDrinkTitle());
       holder.drinkDescription.setText(data.get(position).getDrinkDescription());

       //toto by melo implementovat onclick metodu
       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (onItemClickListener != null) {
                   onItemClickListener.onRecyclerItemClick(v, position, data.get(position));
               }
           }
       });
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {


        TextView drinkTitle;
        TextView drinkDescription;
        int id;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            drinkTitle = itemView.findViewById(R.id.drinkTitle);
            drinkDescription = itemView.findViewById(R.id.drinkDescription);

        }
    }

}
