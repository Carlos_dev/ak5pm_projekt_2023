package cz.utb.fai.project_cocktail_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ListOfFoundActivity extends AppCompatActivity {

    private static final String COCKTAIL_LOOK_API_LINK = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=";
    private static final String COCKTAIL_DRINK_DETALI_API_LINK = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=";

    private String selectedIngredient;

    private boolean Alcoholic = true;

    ArrayList<DrinkDataModel> drinksList = new ArrayList<>();

    DrinkData_recycler_view_adaptor adaptor;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_found);

        selectedIngredient = getIntent().getExtras().getString("selectedIngredient");
        Alcoholic = getIntent().getExtras().getBoolean("nonAlcoholic");

        if (selectedIngredient.isEmpty()) {

            WarningNoIngredientSelected dialog = new WarningNoIngredientSelected();
            dialog.show(getSupportFragmentManager(), "warningNoIngredient");

        }

        else {
            //ingredience byla vybrana

            //kontrola jestli nahodou nemame

            OkHttpClient client = new OkHttpClient();
            Request drinksRequest = new Request.Builder().url(COCKTAIL_LOOK_API_LINK+selectedIngredient).build();

            client.newCall(drinksRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("myTag", "ERR");
                    Log.d("myTag", e.getMessage());

                }

                //priprava seznamu ingredienci
                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if(response.isSuccessful()){
                        String responseData = response.body().string();
//                        Log.d("myTag", COCKTAIL_LOOK_API_LINK+selectedIngredient);

                        //mame nejay string
                        if(!responseData.isEmpty()){

                            //vytvareni pole moznosti pro Spinner
                            try {
                                JSONObject ingredientsJSONResponse;

                                ingredientsJSONResponse = new JSONObject(responseData);

                                JSONArray drinks = (JSONArray) ingredientsJSONResponse.getJSONArray("drinks");

                                int i = 0;
                                int stop = 0;
                                if(drinks.length() > 30) {
                                    //mame vice nez 30, (limit dotazů na API je 60 za 10 skund) -> nejaky random vyber bloku
                                    Random rand = new Random();
                                    i = rand.nextInt(drinks.length() - 30);
                                    stop = i + 30;
                                }
                                else {
                                    i = 0;
                                    stop = drinks.length();
                                }

                                for (; i < stop; i++){

                                    JSONObject drink = (JSONObject) drinks.get(i);

                                    if(((String) drink.get("idDrink")).isEmpty()) {
                                        //neni drink id pokracujeme dale.
                                        continue;

                                    } else {

                                        //ID drinku je, pokracujeme jeho zarazenim do seznamu
                                        Request request = new Request.Builder()
                                            .url(COCKTAIL_DRINK_DETALI_API_LINK+drink.get("idDrink"))
                                            .build();

                                        try (Response cocktailData = client.newCall(request).execute()) {

                                            JSONObject drinkData = new JSONObject(cocktailData.body().string());
                                            JSONArray drinkDataArray = drinkData.getJSONArray("drinks");
                                            drinkData = (JSONObject)drinkDataArray.get(0);


                                            if (Alcoholic == false && drinkData.getString("strAlcoholic").equals("Alcoholic")) {
                                                Log.d("myTag", "NA");

                                                continue;
                                            }

                                            //priprava stringu s ingrediencemi a jejich pomery.
                                            String ingredients = "";
                                            String ingredientsMeasured = "";
                                            for (int j = 1; j < 16; j++) {
                                                if(drinkData.getString("strIngredient"+j).equals("null")) {
                                                    continue;
                                                } else {
                                                    ingredientsMeasured = ingredientsMeasured + drinkData.getString("strIngredient"+j) + ": " + drinkData.getString("strMeasure"+j) + "\n";
                                                    ingredients = ingredients + drinkData.getString("strIngredient"+j) + "; ";
                                                }
                                            }

                                            Log.d("myTag",  ingredients);


                                            drinksList.add(new DrinkDataModel(drinkData.getInt("idDrink"), drinkData.getString("strDrink"), ingredients, ingredientsMeasured, drinkData.getString("strInstructions"), drinkData.getString("strGlass")));


                                        }

                                    }

                                    ListOfFoundActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                           adaptor.notifyDataSetChanged();
                                        }

                                    });



                                    //pro kazdy drink  je potreba dotahnout jeste dalsi informace o drinku

                                    //uz zde je potreba ziskat informace o jednotlivych cocktailech aby

                                }

                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }

                        }

                    }

                    //toto vola update obsahu spinneru po dokocneni callu
//                    LookForCocktailActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            spinnerAdapter.notifyDataSetChanged();
//                        }
//
//                    });

                }

            });

            RecyclerView drinksRecyclerView = findViewById(R.id.drinksRecycler);

            adaptor = new DrinkData_recycler_view_adaptor(this, drinksList);
            //aby tofungovalo, musime intent vytvorit tu
            Intent detailFoFoundDrink = new Intent(this, DrinkDetailFromFoundActivity.class);

            //tady se definuje on click adaptor pro
            adaptor.setOnItemClickListener(new OnRecyclerItemClickInterface() {
                @Override
                public void onRecyclerItemClick(View view, int position, DrinkDataModel data) {
                    Log.d("myTag", "list item clicked");
                    Log.d("myTag", ""+data.getID());

                    //tady je potreba nastartovat novou kativitu s detailem drinku

                    detailFoFoundDrink.putExtra("DrinkDataModel", data);
                    startActivity(detailFoFoundDrink);


                }
            });

            drinksRecyclerView.setAdapter(adaptor);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            drinksRecyclerView.setLayoutManager(manager);



        }





    }


}
