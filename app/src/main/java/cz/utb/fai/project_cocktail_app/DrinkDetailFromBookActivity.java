package cz.utb.fai.project_cocktail_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DrinkDetailFromBookActivity extends AppCompatActivity {

    DrinkDataModel drink;

    TextView title;

    TextView ingredients;

    TextView instructions;
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_detail_from_book);

        drink = (DrinkDataModel)getIntent().getSerializableExtra("DrinkDataModel");

        title = (TextView) findViewById(R.id.drinkTitle);
        title.setText(drink.getDrinkTitle());

        ingredients = (TextView) findViewById(R.id.drinkIngredientsListed);
        ingredients.setText(drink.getIngredientsMeasured());

        instructions = (TextView) findViewById(R.id.howto);
        instructions.setText(drink.getInstructions());

    }

}
