package cz.utb.fai.project_cocktail_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import org.w3c.dom.Text;

public class DrinkDetailFromFoundActivity extends AppCompatActivity {

    DrinkDataModel drink;

    TextView title;

    TextView ingredients;

    TextView instructions;

    Button addToDrinkbook;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_detail_from_found);

        this.drink = (DrinkDataModel)getIntent().getSerializableExtra("DrinkDataModel");

        title = (TextView) findViewById(R.id.drinkTitle);
        title.setText(drink.getDrinkTitle());

        ingredients = (TextView) findViewById(R.id.drinkIngredientsListed);
        ingredients.setText(drink.getIngredientsMeasured());

        instructions = (TextView) findViewById(R.id.howto);
        instructions.setText(drink.getInstructions());

        //zpracovani klku na tlacitko pridani do drinkbooku
        addToDrinkbook = (Button) findViewById(R.id.buttonAddToLibrary);
        addToDrinkbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleAddToDrinkbook();
            }
        });

    }

    public void handleAddToDrinkbook() {

       Gson gson = new Gson();

        //budeme ukladat data z polozky
        SharedPreferences sharedPreferences = getSharedPreferences("drinkbook", Context.MODE_PRIVATE);
        String sharedData = sharedPreferences.getString("drinkbookData", "");

        Log.d("MMM", sharedData);

        //kontrola jestli jiy drink nemame pridany
        if(sharedData.contains("\"ID\":"+this.drink.getID()+",")) {
            Log.d("my trag", "alreadz exists");

            WarningDrinkAlreadyExists dialog = new WarningDrinkAlreadyExists();
            dialog.show(getSupportFragmentManager(), "WarningDrinkAlreadyExists");

        } else {


            String jsonData = gson.toJson(this.drink);
            //pokud jize neco mame uloyeno, je potreba k tomu pristupovat jinak nez pokud ne.
            if (sharedData.length() > 0) {
                sharedData = sharedData + ',' + jsonData;

            } else {
                sharedData = jsonData;
            }

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("drinkbookData", sharedData);
        editor.apply();
        }

    }

}
