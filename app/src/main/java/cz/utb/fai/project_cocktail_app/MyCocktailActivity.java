package cz.utb.fai.project_cocktail_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MyCocktailActivity extends AppCompatActivity {

    ArrayList<DrinkDataModel> drinksList = new ArrayList<>();

    DrinkData_recycler_view_adaptor adaptor;


    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cocktail);


        SharedPreferences sharedPreferences = getSharedPreferences("drinkbook", Context.MODE_PRIVATE);
        String sharedData = sharedPreferences.getString("drinkbookData", "");

        JSONArray jsonDrinks = null;
        try {
            jsonDrinks = new JSONArray("["+sharedData+"]");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        JSONObject drinkData = null;
        Gson gson = new Gson();
        for(int i = 0; i < jsonDrinks.length(); i++) {

            try {
                drinkData = (JSONObject)jsonDrinks.get(i);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            drinksList.add(gson.fromJson(drinkData.toString(), DrinkDataModel.class));
        }

        RecyclerView drinksRecyclerView = findViewById(R.id.drinksRecycler);

        adaptor = new DrinkData_recycler_view_adaptor(this, drinksList);
        //aby tofungovalo, musime intent vytvorit tu
        Log.d("myTag", "KLK");
        Intent detailFoFoundDrink = new Intent(this, DrinkDetailFromBookActivity.class);

        //tady se definuje on click adaptor pro
        adaptor.setOnItemClickListener(new OnRecyclerItemClickInterface() {
            @Override
            public void onRecyclerItemClick(View view, int position, DrinkDataModel data) {

                //tady je potreba nastartovat novou kativitu s detailem drinku

                detailFoFoundDrink.putExtra("DrinkDataModel", data);
                startActivity(detailFoFoundDrink);

            }
        });

        drinksRecyclerView.setAdapter(adaptor);
        drinksRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
