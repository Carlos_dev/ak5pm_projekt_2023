package cz.utb.fai.project_cocktail_app;

import java.io.Serializable;
import java.util.ArrayList;

public class DrinkDataModel implements Serializable {

    protected int ID;

    protected String drinkTitle;

    protected String drinkDescription;

    protected String instructions;

    protected String glass;

    protected boolean isAlcoholic;

    protected String ingredientsMeasured;

    public DrinkDataModel(int ID, String drinkTitle, String drinkDescription, String ingredientsMeasured, String instructions, String glass)  {
        this.ID = ID;
        this.drinkTitle = drinkTitle;
        this.drinkDescription = drinkDescription;
        this.instructions = instructions;
        this.glass = glass;
        this.ingredientsMeasured = ingredientsMeasured;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDrinkTitle() {
        return drinkTitle;
    }

    public void setDrinkTitle(String drinkTitle) {
        this.drinkTitle = drinkTitle;
    }

    public String getDrinkDescription() {
        return drinkDescription;
    }

    public void setDrinkDescription(String drinkDescription) {
        this.drinkDescription = drinkDescription;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getGlass() {
        return glass;
    }

    public void setGlass(String glass) {
        this.glass = glass;
    }

    public boolean isAlcoholic() {
        return isAlcoholic;
    }

    public void setAlcoholic(boolean alcoholic) {
        isAlcoholic = alcoholic;
    }

    public String getIngredientsMeasured() {
        return ingredientsMeasured;
    }

    public void setIngredientsMeasured(String ingredientsMeasured) {
        this.ingredientsMeasured = ingredientsMeasured;
    }
}
