package cz.utb.fai.project_cocktail_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class LookForCocktailActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener{


    private static final String INGREDIENCES_API_LINK = "https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list";

    List<String> availableIngredients = new ArrayList<>();


    private Spinner ingredients;

    ArrayAdapter<String> spinnerAdapter;

    private String selectedIngredient;

    private boolean canBeAlcoholic = true;

    private Button searchButton;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_for_cocktail);

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        //kontrola ze mame pripojeni, pokdu pripojeni nemame, pak se jen udela dialog a nasledne
        if (!isConnected) {

            WarningNoConnection dialog = new WarningNoConnection();
            dialog.show(getSupportFragmentManager(), "warningNoConnection");

        }
        //mame pripojeni, tak je treba yiskat ingredience.
        else {

            OkHttpClient client = new OkHttpClient();
            Request ingredientsRequest = new Request.Builder().url(INGREDIENCES_API_LINK).build();

            client.newCall(ingredientsRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("ErrorTag", e.getMessage());

                }

                //priprava seznamu ingredienci
                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if(response.isSuccessful()){
                        String responseData = response.body().string();

                        //mame nejay string
                        if(!responseData.isEmpty()){

                            //vytvareni pole moznosti pro Spinner
                            try {
                                JSONObject ingredientsJSONResponse;

                                ingredientsJSONResponse = new JSONObject(responseData);

                                JSONArray ingredients = (JSONArray) ingredientsJSONResponse.getJSONArray("drinks");

                                for (int i = 0; i < ingredients.length(); i++){

                                    JSONObject ingredient = (JSONObject) ingredients.get(i);

                                    availableIngredients.add((String)ingredient.get("strIngredient1"));

                                }

                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }

                        }

                    }
                    //toto vola update obsahu spinneru po dokocneni callu
                   LookForCocktailActivity.this.runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                            spinnerAdapter.notifyDataSetChanged();
                       }

                   });

                }

            });

        }

        //ziskani spinneru
        ingredients = findViewById(R.id.ingredients);

        //nahrani moznosti pro spinner
        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  availableIngredients);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //prompt ve spinneru
        ingredients.setAdapter(spinnerAdapter);
        ingredients.setPrompt("Ingredient");

        //nastavime ze lsitenerem bude tato trida
        ingredients.setOnItemSelectedListener(this);

        //ziskani switche - prepinani jestli bereme jen nealkoholicke
        Switch nonAlcoholic = (Switch) findViewById(R.id.nonAlcoholic);

        nonAlcoholic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    canBeAlcoholic = false;
                    // The switch is enabled
                } else {
                    canBeAlcoholic = true;
                }

            }
        });


        searchButton = (Button)findViewById(R.id.getCocktails);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goToListOfFound();
            }
        });



    }


    @Override
    //callbeck po vzbránmí hodnoty ingredience
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        //nastavime hodnotu vybrane ingredience, v dalsich API callech budeme ale  potřebujeme nahradit mezeru podtrzitkem.
        selectedIngredient = availableIngredients.get(position).replace(' ', '_');

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        selectedIngredient = null;
    }

    public void goToListOfFound() {

        Intent listOfFound = new Intent(this, ListOfFoundActivity.class);
        listOfFound.putExtra("nonAlcoholic", canBeAlcoholic);
        listOfFound.putExtra("selectedIngredient", selectedIngredient);

        startActivity(listOfFound);

    }


}
