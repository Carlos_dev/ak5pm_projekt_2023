package cz.utb.fai.project_cocktail_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.splashscreen.SplashScreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    //flag pro splash screen
    boolean splashFinished = false;
    private Button lookForCocktailsButton;
    private Button myCocktailsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //nastavi splash screen ktera bude zobrazena na zacatku
        SplashScreen splashScreen = SplashScreen.installSplashScreen(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ziskani tlacitek podel toho co je
        lookForCocktailsButton = (Button)findViewById(R.id.lookForCocktails);
        myCocktailsButton = (Button)findViewById(R.id.myCocktails);

        //kliknuti na tlacitko pro vyhledavani cocktailu
        lookForCocktailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLookForCocktails();
            }
        });

        //handlovani kliknuti na tlacitko s prehledem ulozenych cocktailu
        myCocktailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToMyCocktails();
            }
        });


        //timto by se melo nastavit pozdrzeni splash screen na nekolik vterin.
        final View content = findViewById(android.R.id.content);

        content.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (splashFinished) {
                    content.getViewTreeObserver().removeOnPreDrawListener(this);
                }
                holdSplash();
                return false;
            }



            //metoda ktera podrzi splash screen po urcenou dobu
            private void holdSplash() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        splashFinished = true;
                    }
                }, 10000);
            }
        });
    }


    public void goToLookForCocktails() {


            Intent lookForCocktails = new Intent(this, LookForCocktailActivity.class);

            startActivity(lookForCocktails);

    }



    public void goToMyCocktails() {

        Intent myCocktails = new Intent(this, MyCocktailActivity.class);

        startActivity(myCocktails);

    }

}