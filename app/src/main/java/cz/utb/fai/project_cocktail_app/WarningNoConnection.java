package cz.utb.fai.project_cocktail_app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatDialogFragment;


public class WarningNoConnection extends AppCompatDialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //vytvoreni noveho dialog builder
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        //nastaveni nadpisu a zpravy
        alertDialog.setTitle("No internet connection");
        alertDialog.setMessage("You are currently not connected to internet, thus we can't find you a cocktail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            //zavrit po kliknuti na Rozumim
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return alertDialog.create();
    }



}
